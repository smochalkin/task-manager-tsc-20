package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove a user by id.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id:");
        String userId = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeById(userId);
    }

}
