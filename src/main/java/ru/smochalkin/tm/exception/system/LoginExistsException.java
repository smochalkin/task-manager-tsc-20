package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Warning! Login already exists...");
    }

}
