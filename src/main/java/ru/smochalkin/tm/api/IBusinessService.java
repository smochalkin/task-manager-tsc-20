package ru.smochalkin.tm.api;

import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    E create(String userId, String name, String description);

    void clear(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    List<E> findAll(String userId);

    E findByName(String userId, String name);

    E findByIndex(String userId, Integer index);

    E removeByName(String userId, String name);

    E removeByIndex(String userId, Integer index);

    E updateById(String id, String name, String desc);

    E updateByIndex(String userId, Integer index, String name, String desc);

    E updateStatusById(String id, Status status);

    E updateStatusByName(String userId, String name, Status status);

    E updateStatusByIndex(String userId, Integer index, Status status);

    boolean isIndex(Integer index);

}
