package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository  extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findTasksByProjectId(final String projectId) {
        List<Task> result = new ArrayList<>();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId())) result.add(task);
        }
        return result;
    }

    @Override
    public void removeTasksByProjectId(final String projectId) {
        List<Task> projectTaskList = findTasksByProjectId(projectId);
        list.removeAll(projectTaskList);
    }

}