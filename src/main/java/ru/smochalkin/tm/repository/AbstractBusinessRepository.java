package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(final String userId){
        List<E> entities = findAll(userId);
        list.removeAll(entities);
    }

    @Override
    public List<E> findAll(final String userId){
        final List<E> entities = new ArrayList<>();
        for (E entity : list) {
            if (entity.getUserId().equals(userId)) entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator){
        List<E> entities = findAll(userId);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findByName(final String userId, final String name) {
        List<E> entities = findAll(userId);
        for (E entity : entities) {
            if (entity.getName().equals(name)) return entity;
        }
        throw new EntityNotFoundException();
    }

    @Override
    public E findByIndex(final String userId, final int index) {
        List<E> entities = findAll(userId);
        return entities.get(index);
    }


    @Override
    public E removeByName(final String userId, final String name) {
        E entity = findByName(userId, name);
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        E entity = findByIndex(userId, index);
        list.remove(entity);
        return entity;
    }

    @Override
    public E updateById(final String id, final String name, final String desc) {
        E entity = findById(id);
        entity.setName(name);
        entity.setDescription(desc);
        return entity;
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String desc) {
        E entity = findByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(desc);
        return entity;
    }

    @Override
    public E updateStatusById(final String id, final Status status) {
        E entity = findById(id);
        entity.setStatus(status);
        updateDate(entity, status);
        return entity;
    }

    @Override
    public E updateStatusByName(final String userId, final String name, final Status status) {
        E entity = findByName(userId, name);
        entity.setStatus(status);
        updateDate(entity, status);
        return entity;
    }

    @Override
    public E updateStatusByIndex(final String userId, final int index, final Status status) {
        E entity = findByIndex(userId, index);
        entity.setStatus(status);
        updateDate(entity, status);
        return entity;
    }

    @Override
    public int getCountByUser(final String userId) {
        List<E> entities = findAll(userId);
        return entities.size();
    }

    private void updateDate(final E entity, final Status status) {
        switch (status) {
            case IN_PROGRESS:
                entity.setStartDate(new Date());
                break;
            case COMPLETED:
                entity.setEndDate(new Date());
                break;
            case NOT_STARTED:
                entity.setStartDate(null);
                entity.setEndDate(null);
        }
    }

}
