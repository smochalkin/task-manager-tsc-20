package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void clear() {
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(list);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findById(final String id) {
        for (E entity : list) {
            if (entity.getId().equals(id)) return entity;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public E remove(final E entity) {
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        return remove(entity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

}

