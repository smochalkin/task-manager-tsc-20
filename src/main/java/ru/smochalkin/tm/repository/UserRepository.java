package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeByLogin(final String login) {
        User user = findByLogin(login);
        return this.remove(user);
    }

    @Override
    public Boolean isLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

}
