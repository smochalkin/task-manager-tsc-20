package ru.smochalkin.tm.util;

public interface ValidateUtil {

    static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

}
