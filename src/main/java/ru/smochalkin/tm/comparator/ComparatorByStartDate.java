package ru.smochalkin.tm.comparator;

import ru.smochalkin.tm.api.model.IHasStartDate;
import java.util.Comparator;

public class ComparatorByStartDate implements Comparator<IHasStartDate> {

    public final static ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    private ComparatorByStartDate(){}

    @Override
    public int compare(IHasStartDate o1, IHasStartDate o2) {
        if (o1.getStartDate() == null) return 1;
        if (o2.getStartDate() == null) return -1;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
